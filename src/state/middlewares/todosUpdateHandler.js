const todoUpdateActions = [
  'todos/add',
  'todos/update',
  'todos/toggleDone',
  'todos/remove',
]

export default handlerFn => store => next => action => {
  const result = next(action)

  if (todoUpdateActions.includes(action.type)) {
    handlerFn(store.getState().todos)
  }

  return result
}
