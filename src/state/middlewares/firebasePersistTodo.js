import { persistTodos } from 'firebaseUtils'
import todosUpdateHandler from './todosUpdateHandler'

export default todosUpdateHandler((state) => {
  persistTodos(state)
})