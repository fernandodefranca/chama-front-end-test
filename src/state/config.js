import todos from './models/todos'
import firebasePersistTodo from './middlewares/firebasePersistTodo'

export default {
  models: {
    todos,
  },
  redux: {
    middlewares: [ firebasePersistTodo, ]
  }
}