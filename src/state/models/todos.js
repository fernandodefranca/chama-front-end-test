const defaultState = {
  list: [],
  isLoading: false,
}

export default {
  state: {
    ...defaultState
  },
  reducers: {
    add: (state, task) => {
      state.list.push({...task, id: Date.now()})
      return { ...state }
    },
    update: (state, taskId, updatedTask) => {
      const list = state.list.map((task) => {
        if (task.id === taskId) {
          task = {
            ...task,
            ...updatedTask
          }
        }
        return task
      })
      return { ...state, list }
    },
    toggleDone: (state, taskId) => {
      const list = state.list.map((task) => {
        if (task.id === taskId) {
          task.done = !task.done
        }
        return task
      })
      return { ...state, list }
    },
    remove: (state, taskId) => {
      const list = state.list.filter((task) => {
        return task.id !== taskId
      })
      return { ...state, list }
    },
    updateLoadingStatus: (state, isLoading) => {
      return { ...state, ...{isLoading}}
    },
    reset: () => {
      return { ...defaultState }
    },
    hydrate: (state, serializedState) => {
      let newState
      try {
        newState = JSON.parse(serializedState)
      } catch (err) {
        newState = {}
      }
      return { ...state, ...newState }
    }
  },
}
