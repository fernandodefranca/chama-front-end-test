export default {
  priorityTypes: [
    { value: 0, label: 'None'},
    { value: 1, label: 'Low'},
    { value: 2, label: 'Medium'},
    { value: 3, label: 'High'},
  ]
}