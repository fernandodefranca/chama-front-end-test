import React from "react"
import { shallow, mount } from "enzyme"
import Component from "../components/TaskInputContainer/TaskInputContainer"

describe("TaskInputContainer component", () => {
  it("Should render without errors without props.", () => {
    const component = mount(<Component />)
  })

  it("Should render the task input with the provided initial state", () => {
    const props = {
      initialState: {
        description: "Adding a task",
        priority: 2,
      }
    }
    const component = mount(<Component {...props} />)
    expect(component.find('.Description input').props().value).toBe(props.initialState.description)
    expect(component.find('select').props().value).toBe(2)
  })
})