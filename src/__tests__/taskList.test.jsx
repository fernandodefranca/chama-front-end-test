import React from "react"
import { shallow, mount } from "enzyme"

import Component from "../components/TaskList/TaskList"

import CallToAction from 'components/CallToAction'
import Progress from 'components/Progress'
import TaskListItem from '../components/TaskListItem/TaskListItem.js'

describe("TaskList component", () => {
  it("Should render without errors without props.", () => {
    const component = mount(<Component />)
  })

  it("Should render the progress component if it is loading results.", () => {
    const props = {
      todos: {
        isLoading: true,
      }
    }
    const component = mount(<Component {...props} />)
    expect(component.find(Progress).length).toBe(1)
  })

  it("Should render the required message when no tasks are available", () => {
    const props = {
      todos: {
        isLoading: false,
        list: []
      }
    }
    const component = mount(<Component {...props} />)
    expect(component.find(CallToAction).length).toBe(1)
    expect(component.find(CallToAction).text()).toBe("You have no tasks at the moment :(")
  })

  it("Should render the expected number of task list items", () => {
    const props = {
      todos: {
        isLoading: false,
        list: [
          { id: 1, description: 'One' },
          { id: 2, description: 'Two' },
          { id: 3, description: 'Three' },
        ]
      }
    }
    const component = mount(<Component {...props} />)
    expect(component.find(TaskListItem).length).toBe(3)
  })
})