import React from "react"
import { shallow, mount } from "enzyme"
import Component from "../components/AppToolbar/AppToolbar"

describe("AppToolbar component", () => {
  it("Should render without errors without props.", () => {
    const component = mount(<Component />)
  })

  it("Should render without errors when props are given.", () => {
    const props = {
      title:"Tasks",
      userEmail:"userEmail",
      userAvatarUrl:"userAvatarUrl",
    }
    const component = mount(<Component {...props} />)
  })

  it("Should contain the given title.", () => {
    const props = {
      title:"Tasks",
    }
    const component = mount(<Component {...props} />)
    expect(component.text()).toBe("Tasks")
  })
})