import React from "react"
import { shallow, mount } from "enzyme"
import Component from "../components/TaskInput/TaskInput"

describe("TaskInput component", () => {
  it("Should render without errors without props.", () => {
    const component = mount(<Component />)
  })

  it("Should contain the default priority.", () => {
    const component = mount(<Component />)
    expect(component.find('select').props().value).toBe(0)
  })

  it("Should display the given description.", () => {
    const desiredString = "My new task"
    const component = mount(<Component description={desiredString} />)
    expect(component.find('.Description input').props().value).toBe(desiredString)
  })

  it("Should display the given priority.", () => {
    const desiredNum = 2
    const component = mount(<Component priority={desiredNum} />)
    expect(component.find('select').props().value).toBe(desiredNum)
  })

  it("Should display the remove button.", () => {
    const props = {
      canRemove:true,
      onRemove: ()=>{},
    }
    const component = mount(<Component {...props} />)
    expect(component.find('button.Remove').length).toBe(1)
  })

  it("Should NOT display the remove button.", () => {
    const component = mount(<Component />)
    expect(component.find('button.Remove').length).toBe(0)
  })
})