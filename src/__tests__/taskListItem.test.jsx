import React from "react"
import { shallow, mount } from "enzyme"
import Component from "../components/TaskListItem/TaskListItem"

describe("TaskListItem component", () => {
  it("Should render without errors without props.", () => {
    const component = mount(<Component />)
  })

  it("Should render the provided props", () => {
    const props = {
      description: "Task Description"
    }
    const component = mount(<Component {...props} />)
    expect(component.find('.Description').at(0).text()).toBe(props.description)
  })

  it("Should display a button to remove", () => {
    const component = mount(<Component />)
    expect(component.find('button.Delete').length).toBe(1)
  })

  it("Should display a button to toggle", () => {
    const component = mount(<Component />)
    expect(component.find('button.Toggle').length).toBe(1)
  })

  it("Should display a button to edit", () => {
    const component = mount(<Component />)
    expect(component.find('button.Edit').length).toBe(1)
  })

  it("Should run the toggle callback with the task id when toggle button is clicked", (done) => {
    const props = {
      id: 333,
      toggleDone: (_id) => {
        expect(_id).toBe(333)
        done()
      }
    }
    const component = mount(<Component {...props} />)
    component.find('button.Toggle').simulate("click")
  })

  it("Should run the remove callback with the task id when remove button is clicked", (done) => {
    const props = {
      id: 444,
      remove: (_id) => {
        expect(_id).toBe(444)
        done()
      }
    }
    const component = mount(<Component {...props} />)
    component.find('button.Delete').simulate("click")
  })

  it("Should run the edit callback with the task id when edit button is clicked", (done) => {
    const props = {
      id: 555,
      onEdit: (_id) => {
        expect(_id).toBe(555)
        done()
      }
    }
    const component = mount(<Component {...props} />)
    component.find('button.Edit').simulate("click")
  })
})