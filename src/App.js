import React from 'react'
import _get from 'lodash.get'

// eslint-disable-next-line
import style from './main.css'
// eslint-disable-next-line
import normalize from 'normalize.css'

import { Provider } from 'react-redux'
import store, { connectComponentWithModels } from 'state'
import { fetchTodos, subscribeForTodosUpdates } from 'firebaseUtils'

import Modal from '@material-ui/core/Modal'
import Button from '@material-ui/core/Button'
import styled from 'styled-components'

import AppToolbar from 'components/AppToolbar'
import TaskInputContainer from 'components/TaskInputContainer'
import TaskList from 'components/TaskList'

const ModalWrapper = styled.div`
  top: 50%;
  left: 50%;
  min-width: 85%;
  transform: translate(-50%, -50%);
  position: absolute;
  background: yellow;
`

const AccessoriesWrapper = styled.div`
  margin-right: 16px;
`

class App extends React.Component {
  state = {
    userAvatarUrl: null,
    modalOpen: false,
    modalContent: null,
  }

  componentDidMount() {
    this.hydrateAndSubscribeTodos()
  }

  hydrateAndSubscribeTodos() {
    const { firebase, $todos } = this.props

    if (!firebase) return

    $todos.updateLoadingStatus(true)

    fetchTodos()
      .then((fetchedState) => {
        $todos.hydrate(fetchedState)
        $todos.updateLoadingStatus(false)

        subscribeForTodosUpdates((updatedState) => {
          $todos.hydrate(updatedState)
        })
      })
      .catch((err) => {
        $todos.updateLoadingStatus(false)
      })
  }

  handleLogout = () => {
    const { firebase } = this.props

    firebase && firebase.auth().signOut()
      .then((res) => {})
      .catch((err) => {})
  }

  handleAddNewTaskButton = () => {
    const content = (
      <TaskInputContainer
        submitButtonLabel="Add new task"
        onSubmit={(taskObj)=>{ 
          this.props.$todos.add(taskObj)
          this.closeModal()
        }} />
    )
    this.openModal(content)
  }

  openModal = (content) => {
    this.setState({
      modalOpen: true,
      modalContent: content,
    })
  }

  closeModal = () => {
    this.setState({
      modalOpen: false,
      modalContent: null,
    })
  }

  handleEditTask = (taskId) => {
    const task = this.props.todos.list.filter(task => task.id === taskId)[0]
    if (!task) return

    const someContent = (
      <TaskInputContainer
        submitButtonLabel="Update task"
        onSubmit={(taskObj)=>{
          this.props.$todos.update(taskObj.id, taskObj) 
          this.closeModal()
        }}
        initialState={task}
      />
    )
    this.openModal(someContent)
  }

  render() {
    const { firebase } = this.props

    let userEmail
    let userAvatarUrl

    if (firebase) {
      userEmail = _get(firebase.auth(), 'currentUser.email')
      userAvatarUrl = _get(firebase.auth(), 'currentUser.photoURL')
    }

    return (
      <div>
        <AppToolbar
          onLogout={this.handleLogout}
          title="Tasks"
          accessories={(
            <AccessoriesWrapper>
              <Button onClick={this.handleAddNewTaskButton} color="primary">
                Add task
              </Button>
            </AccessoriesWrapper>
          )}
          userEmail={userEmail}
          userAvatarUrl={userAvatarUrl} />
        <TaskList onEdit={this.handleEditTask} />
        <Modal
          open={this.state.modalOpen}
          onClose={this.closeModal}
        >
          <ModalWrapper>
            {this.state.modalContent}
          </ModalWrapper>
        </Modal>
      </div>
    )
  }
}

const AppConnected = connectComponentWithModels(App, ['todos'])
const AppWithProvider = (props) => (
  <Provider store={store}>
    <AppConnected {...props} />
  </Provider>
)

export default AppWithProvider