import React from 'react'
import ReactDOM from 'react-dom'
import FirebaseAuth from 'components/FirebaseAuth'

import App from './App'
import Welcome from 'components/Welcome'

const AppWithAuth = () => (
  <FirebaseAuth welcomeView={Welcome}>
    <App/>
  </FirebaseAuth>
)

ReactDOM.render(<AppWithAuth />, document.getElementById('app'))