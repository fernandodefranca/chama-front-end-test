import firebase from 'firebase'

const tableName = 'todos'

export function getDatabaseRef(_ref) {
  if (_ref) return firebase.database().ref(_ref)
  return firebase.database().ref()
}

export function getUserId() {
  return firebase.auth().currentUser.uid
}

export function fetchTodos() {
  const userId = getUserId()

  return getDatabaseRef(`/${tableName}/${userId}`)
    .once('value')
    .then((snapshot) => snapshot.val())
    .catch((err) => {
      // eslint-disable-next-line
      console.error('fetchTodos() failed:', err)
    })
}

export function subscribeForTodosUpdates(callbackFn) {
  const userId = getUserId()

  getDatabaseRef(`/${tableName}/${userId}`)
    .on('value', (snapshot) => callbackFn(snapshot.val()))
}

export function persistTodos(state) {
  if (!state) return
  
  const userId = getUserId()
  if (!userId) return

  var updates = {
    [`/${tableName}/${userId}`]: JSON.stringify(state)
  }

  getDatabaseRef().update(updates)
    .catch((err) => {
      // eslint-disable-next-line
      console.error('persistTodos() failed:', err)
    })
}