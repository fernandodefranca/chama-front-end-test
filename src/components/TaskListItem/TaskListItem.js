import React, { Component } from 'react'
import styled from 'styled-components'

import { grey, orange, red } from '@material-ui/core/colors'
import IconButton from '@material-ui/core/IconButton'
import DeleteIcon from '@material-ui/icons/Delete'
import EditIcon from '@material-ui/icons/Edit'
import CheckBoxIcon from '@material-ui/icons/CheckBox'
import CheckBoxOutlineBlankIcon from '@material-ui/icons/CheckBoxOutlineBlank'

import app from 'constants/app'

const priorityColors = [
  grey[500],
  orange[300],
  orange[800],
  red[800],
]

const Wrapper = styled.div`
  padding: 8px 8px;
  background-color: #fff;
  border-bottom: 1px solid #ddd;
  display: flex;
  flex-shrink: 0;
  flex-direction: row;
  user-select: none;

  @media (min-width: 768px) {
    padding: 16px 32px;
  }
`

const Bubble = styled.span`
  background: ${props => priorityColors[props.priority]};
  color: #fff;
  font-size: 11px;
  padding: 4px;
  border-radius: 4px;
  margin: 4px 8px 4px 4px;
`

const FlexGrow = styled.div`
  flex-grow: 1;
  align-items: center;
  display: flex;
`

const DueDate = styled.div`
  font-size: 12px;
  color: ${props => props.isDue ? red[800] : '#777'};
  font-weight: ${props => props.isDue ? 'bold' : 'normal'};
`

const Description = styled.div`
  text-decoration: ${props => props.done ? 'line-through' : 'none'};
`

let priorityTypes = {}
app.priorityTypes.forEach((item) => {
  priorityTypes[item.value] = item.label
})

class TaskListItem extends Component {
  handleToggleDoneClick = () => {
    const { toggleDone } = this.props
    toggleDone && toggleDone(this.props.id)
  }
  
  handleRemoveClick = () => {
    const { remove } = this.props
    remove && remove(this.props.id)
  }

  handleEditClick = () => {
    const { onEdit } = this.props
    onEdit && onEdit(this.props.id)
  }

  render() {
    const {
      description,
      priority,
      dueDate,
      done,
    } = this.props

    const priorityLabel = priorityTypes[priority]
    const dueDateObj = new Date(dueDate)
    const now = Date.now()
    const isDue = dueDateObj.getTime() < now

    return (
      <Wrapper>
        <IconButton
          className="Toggle"
          onClick={this.handleToggleDoneClick}
          aria-label="Toggle">
          {
            done ? (<CheckBoxIcon />) : (<CheckBoxOutlineBlankIcon />)
          }
        </IconButton>
        <FlexGrow>
          <Bubble priority={priority}>{priorityLabel}</Bubble>
          <Description done={done} className="Description">
            {description}
            {
              dueDate &&
              (
                <DueDate isDue={isDue}>
                  {dueDateObj.toLocaleString()}
                </DueDate>
              )
            }
          </Description>
        </FlexGrow>
        <IconButton
          onClick={this.handleEditClick}
          className="Edit"
          aria-label="Edit">
          <EditIcon />
        </IconButton>
        <IconButton
          onClick={this.handleRemoveClick}
          className="Delete"
          aria-label="Delete">
          <DeleteIcon />
        </IconButton>
      </Wrapper>
    )
  }
}
export default TaskListItem