import styled from 'styled-components'

const CallToAction = styled.h3`
  text-align: center;
  padding: 48px;
`

export default CallToAction