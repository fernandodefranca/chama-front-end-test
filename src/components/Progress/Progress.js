import React, { Component } from 'react'
import CircularProgress from '@material-ui/core/CircularProgress'

class Progress extends Component {
  render() {
    return (
      <CircularProgress {...this.props}/>
    )
  }
}
export default Progress