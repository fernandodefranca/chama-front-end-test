import React from 'react'
import styled from 'styled-components'

import AppBar from '@material-ui/core/AppBar'
import Toolbar from '@material-ui/core/Toolbar'
import Typography from '@material-ui/core/Typography'
import IconButton from '@material-ui/core/IconButton'
import AccountCircle from '@material-ui/icons/AccountCircle'
import Menu from '@material-ui/core/Menu'
import MenuItem from '@material-ui/core/MenuItem'
import { grey } from '@material-ui/core/colors'

const Avatar = styled.div`
  background: #f00;
  width: 36px;
  height: 36px;
  border-radius: 36px;
  background-size: cover;
`

const UserDescription = styled.div`
  padding: 16px;
  font-size: 12px;
  color: ${grey[500]};
`

const anchorOrigin = {
  vertical: 'top',
  horizontal: 'right',
}

const transformOrigin = {
  vertical: 'top',
  horizontal: 'right',
}

class AppToolbar extends React.Component {
  state = {
    anchorEl: null,
  }

  handleChange = (event) => {
    this.setState({ auth: event.target.checked })
  };

  handleMenu = (event) => {
    this.setState({ anchorEl: event.currentTarget })
  };

  handleClose = () => {
    this.setState({ anchorEl: null })
  };

  handleLogoutButton = () => {
    const { onLogout } = this.props
    this.handleClose()
    onLogout && onLogout()
  }

  render() {
    const { title, userAvatarUrl, userEmail, accessories } = this.props
    const { anchorEl } = this.state

    const open = Boolean(anchorEl)
    const bgImage = `url("${userAvatarUrl}")`

    return (
      <div className="AppToolbar">
        <AppBar position="static" style={{backgroundColor: grey[200]}}>
          <Toolbar>
            <Typography variant="title" style={{color: grey[800]}} className="flexGrow1">
              {title}
            </Typography>
            <React.Fragment>
              {accessories}
            </React.Fragment>
            <div>
              <IconButton
                aria-owns={open ? 'menu-appbar' : null}
                aria-haspopup="true"
                onClick={this.handleMenu}
                color="inherit"
              >
                {
                  userAvatarUrl ? 
                    <Avatar style={{backgroundImage:bgImage}} /> :
                    <AccountCircle />
                }
              </IconButton>
              <Menu
                id="menu-appbar"
                anchorEl={anchorEl}
                anchorOrigin={anchorOrigin}
                transformOrigin={transformOrigin}
                open={open}
                onClose={this.handleClose}
              >
                <MenuItem onClick={this.handleLogoutButton}>Logout</MenuItem>
                { 
                  userEmail &&
                  (<UserDescription>{userEmail}</UserDescription>)
                }
              </Menu>
            </div>
          </Toolbar>
        </AppBar>
      </div>
    )
  }
}

export default AppToolbar