import React, { Component } from 'react'
import orderBy from 'lodash.orderby'
import _get from 'lodash.get'

import Progress from 'components/Progress'
import TaskListItem from 'components/TaskListItem'

import CallToAction from 'components/CallToAction'

class TaskList extends Component {
  render() {
    const { onEdit } = this.props
    const todos = _get(this.props, 'todos', {})
    const $todos = _get(this.props, '$todos', {})

    const { isLoading } = todos
    const hasError = !todos || !todos.list
    const hasNoTasksYet = !isLoading && !hasError && todos.list.length === 0

    return (
      <div className="TaskList">
        { isLoading && 
          <CallToAction>
            <Progress />
          </CallToAction>
        }
        {
          !hasError && 
          orderBy(todos.list, ['priority', 'dueDate'], ['desc', 'asc'])
            .map((item) => {
              return (
                <TaskListItem
                  {...item}
                  toggleDone={$todos.toggleDone}
                  remove={$todos.remove}
                  onEdit={onEdit}
                  key={item.id} />
              )
            })
        }
        {
          hasNoTasksYet &&
          <CallToAction className="NoTasksYet">You have no tasks at the moment :(</CallToAction>
        }
      </div>
    )
  }
}
export default TaskList