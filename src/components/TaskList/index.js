import Component from './TaskList.js'
import { connectComponentWithModels } from 'state'

export default connectComponentWithModels(Component, ['todos'])