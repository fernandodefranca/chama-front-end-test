import React from 'react'
import StyledFirebaseAuth from 'react-firebaseui/StyledFirebaseAuth'
import firebase from 'firebase'
import firebaseConfig from 'constants/firebaseConfig'

import CallToAction from 'components/CallToAction'

firebase.initializeApp(firebaseConfig)

export default class FirebaseLogin extends React.Component {
  state = {
    isSignedIn: false
  };

  uiConfig = {
    signInFlow: 'popup',
    signInOptions: [
      firebase.auth.GoogleAuthProvider.PROVIDER_ID,
    ],
    callbacks: {
      signInSuccessWithAuthResult: () => false
    }
  };

  // Listen to the Firebase Auth state and set the local state.
  componentDidMount() {
    this.unregisterAuthObserver = firebase
      .auth()
      .onAuthStateChanged(
        (user) => this.setState({
          isSignedIn: !!user,
        })
      )
  }
  
  componentWillUnmount() {
    this.unregisterAuthObserver()
  }

  render() {
    const { isSignedIn } = this.state
    const { welcomeView } = this.props
    
    if (!isSignedIn) {
      return (
        <div>
          { welcomeView && welcomeView() }
          <CallToAction>
            Please sign-in :)
          </CallToAction>
          <StyledFirebaseAuth uiConfig={this.uiConfig} firebaseAuth={firebase.auth()} />
        </div>
      )
    }

    const { children } = this.props

    const childrenWithProps = React
      .Children
      .map(children, (child) => {
        return React.cloneElement(child, { firebase: firebase })
      })

    return <div>{childrenWithProps}</div>
  }
}