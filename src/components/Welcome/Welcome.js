import React from 'react'
import styled from 'styled-components'

const WelcomeWrapper  = styled.div`
  text-align: center;
  font-size: 2rem;
  color: #4ba4bd;
`

const Welcome = () => (
  <WelcomeWrapper>
    <h1>To Do list</h1>
  </WelcomeWrapper>
)

export default Welcome