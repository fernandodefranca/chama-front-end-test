import React, { Component } from 'react'
import PropTypes from 'prop-types'
import styled from 'styled-components'
import FormControl from '@material-ui/core/FormControl'
import TextField from '@material-ui/core/TextField'
import NativeSelect from '@material-ui/core/NativeSelect'
import Input from '@material-ui/core/Input'
import Button from '@material-ui/core/Button'
import FormHelperText from '@material-ui/core/FormHelperText'
import DatePicker from 'react-datepicker'
import moment from 'moment'
import 'react-datepicker/dist/react-datepicker.css'

import app from 'constants/app'

const InnerContent = styled.div`
  background: #fff;
  padding: 32px;
`

class TaskInput extends Component {
  handleChange = (event) => {
    const { name, value } = event.target
    const { onChange } = this.props
    onChange && onChange(name, value)
  }

  handleDateTimeChange = (value) => {
    const { onChange } = this.props
    if (value) {
      return onChange && onChange('dueDate', value.toDate())
    }
    onChange('dueDate', null)
  }

  handleSelectChange = name => event => {
    const { onChange } = this.props
    onChange && onChange(name, event.target.value)
  }

  handleSubmit = (event) => {
    event.preventDefault()
    const { onSubmit } = this.props
    onSubmit && onSubmit()
  }

  render() {
    const { 
      description,
      priority,
      dueDate,
      canSubmit,
      submitButtonLabel,
      onRemove,
      canRemove,
    } = this.props

    let momentDate
    if (dueDate) momentDate = moment(dueDate)

    return (
      <form className="TaskInput" onSubmit={this.handleSubmit}>
        <InnerContent>
          <FormControl fullWidth>
            <TextField
              className="Description"
              value={description}
              name="description"
              onChange={this.handleChange}
              placeholder="Add the task description here"
              autoFocus
            />
            <FormHelperText>Task description</FormHelperText>
          </FormControl>
          <div className="marginTop16">
            <div className="displayInlineBlock marginRight16">
              <NativeSelect
                className="Priority"
                value={priority}
                onChange={this.handleSelectChange('priority')}
                input={<Input name="priority" id="priority-native-helper" />}
              >
                {
                  app.priorityTypes.map((item, i) => {
                    const { value, label } = item
                    return (<option key={i} value={value}>{label}</option>)
                  })
                }
              </NativeSelect>
              <FormHelperText>Priority</FormHelperText>
            </div>
            <div className="displayInlineBlock">
              <DatePicker
                selected={momentDate}
                onChange={this.handleDateTimeChange}
                showTimeSelect
                timeFormat="HH:mm"
                timeIntervals={15}
                dateFormat="DD/MM/YY, h:mm"
                placeholderText="Click to select a date"
                isClearable={true}
                shouldCloseOnSelect={true}
                timeCaption="time" />
              <FormHelperText>Due to</FormHelperText>
            </div>
          </div>
          <div className="marginTop16">
            <Button className="Submit" type="submit" variant="contained" disabled={!canSubmit} color="primary">
              {submitButtonLabel}
            </Button>
            {
              canRemove &&
              onRemove &&
              (
                <span className="marginLeft16">
                  <Button className="Remove" variant="contained" color="secondary" onClick={()=>{onRemove()}}>
                    Remove
                  </Button>
                </span>
              )
            }
          </div>
        </InnerContent>
      </form>
    )
  }
}

TaskInput.propTypes = {
  id: PropTypes.string,
  description: PropTypes.string,
  priority: PropTypes.number,
  dueDate: PropTypes.instanceOf(Date),
  onChange: PropTypes.func,
  onSubmit: PropTypes.func,
  canSubmit: PropTypes.bool,
  submitButtonLabel: PropTypes.string,
  onRemove: PropTypes.func,
  canRemove: PropTypes.bool,
}

TaskInput.defaultProps = {
  description: '',
  priority: 0,
  canSubmit: false,
  submitButtonLabel: 'update',
  canRemove: true,
}

export default TaskInput