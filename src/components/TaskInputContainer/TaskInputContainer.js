import React, { Component } from 'react'
import PropTypes from 'prop-types'
import TaskInput from 'components/TaskInput'

const defaultState = {
  description: '',
  priority: 0,
  dueDate: null,
  done: false,
}

class TaskInputContainer extends Component {
  constructor(props){
    super(props)

    this.state = {
      ...defaultState,
      ...props.initialState
    }
  }

  handleChange = (field, value) => {
    if (field === 'priority') {
      value = Number(value)
    }
    this.setState({[field]: value})
  }

  handleSubmit = () => {
    const { onSubmit } = this.props
    onSubmit && onSubmit(this.state)
    this.setState({...defaultState})
  }

  render() {
    const { description, priority, dueDate, } = this.state
    const { canRemove, onRemove, onEdit, submitButtonLabel } = this.props
    const canSubmit = !!description

    return (
      <div className="TaskInputContainer">
        <TaskInput 
          description={description}
          priority={priority}
          dueDate={dueDate}
          onEdit={onEdit}
          canSubmit={canSubmit}
          onSubmit={this.handleSubmit}
          submitButtonLabel={submitButtonLabel}
          canRemove={canRemove}
          onRemove={onRemove}
          onChange={this.handleChange} />
      </div>
    )
  }
}

TaskInput.propTypes = {
  onSubmit: PropTypes.func,
  initialState: PropTypes.object,
}

export default TaskInputContainer